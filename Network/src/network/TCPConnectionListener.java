package network;

import java.io.IOException;

public interface TCPConnectionListener {

    void onConnectionReady(network.TCPConnection tcpConnection);

    void onReceiveString(network.TCPConnection tcpConnection, String value);

    void onDisconnect(network.TCPConnection tcpConnection);

    void onException(network.TCPConnection tcpConnection, IOException e);

}